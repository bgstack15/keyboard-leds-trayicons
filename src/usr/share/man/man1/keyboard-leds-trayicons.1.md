keyboard-leds-trayicons 1 "March 2020" keyboard-leds-trayicons "User Manual"
================================================================
# NAME
keyboard-leds-trayicons - show capslock and numlock indicators in tray
# SYNOPSIS
keyboard-leds-trayicons
# DESCRIPTION
Show capslock and numlock indicator icons in the panel tray. These can substitute for the keyboard LED icons for keyboards that are missing those features.  
No command-line parameters exist.
# AUTHOR
bgstack15
# REPORTING BUGS
Bug tracker: `<https://gitlab.com/bgstack15/keyboard-leds-trayicons>`  
# COPYRIGHT
Copyright (C) 2020 bgstack15. License CC-BY-SA 4.0.  
This is free software: you are free to change it and redistribute it. There is NO WARRANTY, to the extent permitted by law.
# SEE ALSO
keyboard-leds-trayicons.conf(5)
