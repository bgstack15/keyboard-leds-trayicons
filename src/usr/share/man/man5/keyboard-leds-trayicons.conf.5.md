keyboard-leds-trayicons.conf 5 "March 2020" keyboard-leds-trayicons "File Formats and Conventions"
================================================================
# NAME
keyboard-leds-trayicons.conf - the configuration file for keyboard-leds-trayicons
# FILE FORMAT
The file consists of key-value pairs, and is used primarily for changing what icons are displayed in the system tray. The file will be parsed in a manner similar to shell dot-sourcing the file.

*key*=*value*

Icon names will be resolved according to the xdg specification for icon lookups `[1]` as they will be sent through gtk3.

# FULL EXAMPLE

```
KLT_CAPS_ON_ICON=capslock-on
KLT_CAPS_OFF_ICON=capslock-off
KLT_NUM_ON_ICON=numlock-on
KLT_NUM_OFF_ICON=numlock-off
KLT_KILLFILE="/var/run/user/$( id -u )/kill-all-leds-trayicons"
```

# DEFAULT ORDER
The various config files, by default, are read in this order. First value defined take precedence, so once a variable is defined, it will not be used by a later file.

1. File named in environment variable *KLT_CONF*
2. $HOME/.config/keyboad-leds-trayicons.conf
3. /etc/keyboard-leds-trayicons.conf

# AUTHOR
bgstack15
# REPORTING BUGS
Bug tracker: `<https://gitlab.com/bgstack15/keyboard-leds-trayicons>`  
# COPYRIGHT
Copyright (C) 2020 bgstack15. License CC-BY-SA 4.0.  
This is free software: you are free to change it and redistribute it. There is NO WARRANTY, to the extent permitted by law.
# SEE ALSO
keyboard-leds-trayicons(1)  
[1] `https://specifications.freedesktop.org/icon-theme-spec/icon-theme-spec-latest.html#icon_lookup`
