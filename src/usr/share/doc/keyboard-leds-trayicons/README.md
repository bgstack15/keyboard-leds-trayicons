# Readme for keyboard-leds-trayicons

This tool, written in POSIX shell, places two icons in the notification area (aka system tray), that substitute for the keyboard LEDs for Capslock and Numlock.

## How to configure
You can set environment variable `KLT_CONF` to point to a config file, which gets loaded in a style similar to dot-sourcing a shell script. Existing environment variables will be preserved. `Keyboard-leds-trayicons` will already load the global values from `/etc/keyboard-leds-trayicons.conf` and from your user settings in `$HOME/.config/keyboard-leds-trayicons.conf`.

Here is example config file.

    KLT_CAPS_ON_ICON=capslock-on
    KLT_CAPS_OFF_ICON=capslock-off
    KLT_NUM_ON_ICON=numlock-on
    KLT_NUM_OFF_ICON=numlock-off

As a side effect of using `mktrayicon`, you can use an icon name that follows the [xdg spec](https://www.freedesktop.org/wiki/Specifications/icon-theme-spec/) or you can point to a specific filename. This project includes some simple letter icons in svg format.

## Project license
[CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

## Dependencies
Package manager | packages
--------------- | ------------------------------------------------------------------
binary name     | [mktrayicon](https://gitlab.com/bgstack15/mktrayicon), awk, xset
deb             | [mktrayicon](https://build.opensuse.org/package/show/home:bgstack15/mktrayicon), mawk | gawk, x11-xserver-utils
rpm             | undefined

## Credits
Bundled icons are used under license from Icon8.
* little `c`: [https://visualpharm.com/free-icons/c%20letter-595b40b65ba036ed117d1027](https://visualpharm.com/free-icons/c%20letter-595b40b65ba036ed117d1027)
* License for icons: Use for free, but link to [icons8](https://icons8.com/license)
